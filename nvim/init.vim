"*****************************************************************************
if has('vim_starting')
	  set nocompatible               " Be iMproved
endif

									 let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')

let g:vim_bootstrap_langs =  "html,markdown"
let g:vim_bootstrap_editor = "nvim"				" nvim or vim

if !filereadable(vimplug_exists)
	echo 	"Installing Vim-Plug..."
	  echo ""
	    silent !\curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
		  let g:not_finish_vimplug = "yes"

		    autocmd VimEnter * PlugInstall
endif

" Required:
call plug#begin(expand('~/.config/nvim/plugged'))

" Плагины
Plug 'reedes/vim-pencil'
Plug 'mhinz/vim-startify'
Plug 'Raimondi/delimitMate'
Plug 'sheerun/vim-polyglot'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp', { 'do': 'pip install -r requirements.txt' }
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'roxma/vim-tmux-clipboard'
Plug 'tmux-plugins/vim-tmux'
Plug 'plasticboy/vim-markdown'
Plug 'kshenoy/vim-signature'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'savq/melange'
Plug 'lifepillar/vim-wwdc17-theme'
Plug 'psliwka/vim-smoothie'
Plug 'ryanoasis/vim-devicons'
Plug 'rose-pine/neovim'
Plug 'quarto-dev/quarto-nvim'
Plug 'jmbuhr/otter.nvim'
Plug 'hrsh7th/nvim-cmp'
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'preservim/tagbar'
"Plug 'Exafunction/codeium.vim'
call plug#end()

autocmd BufEnter * call ncm2#enable_for_buffer()

syntax on
filetype indent on
filetype plugin on

colorscheme rose-pine " melange rose-pine wwdc17

let g:wwdc17_frame_color = 10
let g:wwdc17_transp_bg = 1

"НАСТРОЙКИ ГОРЯЧИХ КЛАВИШ
" Сохранить файл
map <F2> :w<cr>
" Выйти из редактора
map <F3> :q<CR>
" Выйти не сохраняя файл
map <F4> :q!<CR>
" Создаёт вертикальные и горизонтальные окна
map <F5> :vsplit<CR>  " Создаёт справа дополнительное окно, сплит
map <F6> :split<CR>   " Создаёт сверху дополнительное окно, сплит
" Включение и выключение номеров строк
"map <F7> :set number relativenumber <CR>  " включает номера строк
"map <F8> :set nonumber norelativenumber <CR> " отключает номера строк
nmap <F8> :TagbarToggle<CR>

" Переключение между окнами
map <C-h> <C-w>h	 " Ctrl+h - Направо
map <C-j> <C-w>j	 " Ctrl+j - Вверх
map <C-k> <C-w>k	 " Ctrl+k - Вниз
map <C-l> <C-w>l	 " Ctrl+l - Налево

" Обеспечивает передвижение курсора в режиме вставки с помощью клавиши-модификатора <Alt>
inoremap <A-h> <C-o>h
inoremap <A-j> <C-o>j
inoremap <A-k> <C-o>k
inoremap <A-l> <C-o>l

set background=light  " бекграунд тёмный (dark) и светлый (light)
set showtabline=2 " Показывает номер вкладки и помогает в перемещении по вкладкам
set noshowmode " Отключить --Вставить--
set laststatus=1
set t_Co=256 " Использовать все 256 цветов в терминале
set tabstop=2 " отступ Tab в 4 пробела
set shiftwidth=2
set expandtab softtabstop=2
set autoindent smartindent
set ignorecase
set smartcase
"set wrap
autocmd BufRead,BufNewFile *.md set wrap
set linebreak
set showbreak=↪
set mps+=<:> " показывать совпадающие скобки для HTML-тегов
set history=200 " хранить больше истории команд
set whichwrap+=<,>,h,l
autocmd CursorMoved * silent! exe printf("match Search /\\<%s\\>/", expand('<cword>'))   " поиск выделенного текста начинает искать фрагмент при его выделении
vnoremap <silent>* <ESC>:call VisualSearch()<CR>/<C-R>/<CR>   " Поиск слова в тексте сверху вниз по текту
vnoremap <silent># <ESC>:call VisualSearch()<CR>?<C-R>/<CR>   " Поиск слова в тексте снизу вверх
set cursorline " Подсветка текущей строки
hi CursorLine term=bold cterm=NONE ctermbg=none  ctermfg=none gui=bold
hi CursorLineNr term=bold cterm=none ctermbg=none ctermfg=white gui=bold
set termguicolors " Управление цветом
set bomb " Кодировка текста
set binary " Автоматическое преобразоване файла в 16ный режим
set backup " Бэкап включён
set undodir=~/.vim/.undo//
set backupdir=~/.vim/.backup//
set directory=~/.vim/.swp//
set fileformats=unix,dos
autocmd BufEnter * :set number relativenumber
set modelines=10
set title titlestring=%<%F%=%l/%L-%P titlelen=70 
set visualbell
set clipboard+=unnamedplus
" hi iCursor guifg=white guibg=red
" set guicursor=n-v-c:block-Cursor
" set guicursor+=i:ver100-iCursor
" set guicursor+=n-v-c:blinkon0
" set guicursor+=i:blinkwait10
set shell=/usr/bin/zsh
set completeopt=noinsert,menuone,noselect
set complete=.,w,b,u,t,k,kspell
set shortmess+=c
set conceallevel=0
set textwidth=0
set wrapmargin=0
set updatetime=100
set statusline=%<%f\ %h%m%r%w\ \ %{PencilMode()}\ %=\ col\ %c%V\ \ line\ %l\,%L\ %P
set rulerformat=%15.(%l,%c%V%)%{PencilMode()}\ %P
set ttimeoutlen=10
set wildmode=longest,full
set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
inoremap <C-l> <C-^>
set encoding=utf-8
set termencoding=utf-8
set fileencoding=utf-8
set fileencodings=ucs-bom,utf-8,big5,gb2312,latin1,cp936
set splitbelow
set splitright
set backspace=indent,eol,start
set binary
set noeol
set exrc
set secure
set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz
set langmap+=ЖжЭэХхЪъ;\:\;\"\'{[}]
set list listchars+=eol:•,tab:→→,trail:↲,nbsp:↔

let mapleader = " "
let g:netrw_banner=0

" Markdown

augroup pencil
  autocmd!
  autocmd FileType markdown,mkd,md,qmd call pencil#init()
                            \ | setl spell spl=ru_yo fdl=4 noru nonu nornu
                            \ | setl fdo+=search
  autocmd Filetype mail         call pencil#init({'wrap': 'hard'})
                            \ | setl spell spl=ru_yo et sw=2 ts=2 noai nonu nornu
  autocmd Filetype html,xml     call pencil#init({'wrap': 'soft'})
                            \ | setl spell spl=en_us et sw=2 ts=2
augroup END

let g:vim_markdown_conceal=1 
autocmd Bufread *.md,*.qmd  setlocal conceallevel=0

let g:airline_section_x = '%{PencilMode()}'
let g:pencil#autoformat = 1 " 0=disable, 1=enable (def)
let g:pencil#map#suspend_af = 'K'
let g:pencil#textwidth = 300
let g:pencil#joinspaces = 0
let g:pencil#cursorwrap = 1
let g:pencil#conceallevel = 3     " 0=disable, 1=one char, 2=hide char, 3=hide all (def)
let g:pencil#concealcursor = 'c'  " n=normal, v=visual, i=insert, c=command (def)
let g:pencil#mode_indicators = {'hard': 'H', 'auto': 'A', 'soft': 'S', 'off': '',}
let g:pencil#softDetectSample = 20
let g:pencil#softDetectThreshold = 130
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0
let g:tex_conceal = ""
let g:vim_markdown_math = 1
let g:vim_markdown_frontmatter = 1  " for YAML format
let g:vim_markdown_toml_frontmatter = 1  " for TOML format
let g:vim_markdown_json_frontmatter = 1  " for JSON format"

" Проверка орфографии -->
setlocal spell spelllang=ru_yo,en_us
set spellfile=~/.config/nvim/spell/ru.utf-8.add
set spellfile=~/.config/nvim/spell/en.utf-8.add
set spell
menu Spell.off :setlocal spell spelllang=<CR>:setlocal nospell<CR>
menu Spell.Russian+English :setlocal spell spelllang=ru,en<CR>
menu Spell.Russian :setlocal spell spelllang=ru<CR>
menu Spell.English :setlocal spell spelllang=en<CR>
menu Spell.-SpellControl- :
menu Spell.Word\ Suggest<Tab>z= z=
menu Spell.Add\ To\ Dictionary<Tab>zg zg
menu Spell.Add\ To\ TemporaryDictionary<Tab>zG zG
menu Spell.Remove\ From\ Dictionary<Tab>zw zw
menu Spell.Remove\ From\ Temporary\ Dictionary<Tab>zW zW
menu Spell.Previous\ Wrong\ Word<Tab>[s [s
menu Spell.Next\ Wrong\ Word<Tab>]s ]s
" Проверка орфографии <--

" Создание вкладок (табы)
nnoremap <Tab> gt
nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

au User Ncm2Plugin call ncm2#register_source({
            \ 'name' : 'css',
            \ 'priority': 9,
            \ 'subscope_enable': 1,
            \ 'scope': ['css','scss'],
            \ 'mark': 'css',
            \ 'word_pattern': '[\w\-]+',
            \ 'complete_pattern': ':\s*',
            \ 'on_complete': ['ncm2#on_complete#omni', 'csscomplete#CompleteCSS'],
            \ })

" use alt+hjkl to move between split/vsplit panels
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l

nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" Startify

" Don't change to directory when selecting a file
let g:startify_files_number = 7
let g:startify_change_to_dir = 0
let g:startify_custom_header = [ ]
let g:startify_relative_path = 0
let g:startify_use_env = 1

" Custom startup list, only show MRU from current directory/project
let g:startify_lists = [
			\ { 'type': 'files',      'header': ['   Last recently opened files:']            },
			\  { 'type': 'dir',       'header': ['   Files from: '. getcwd()] },
			\  { 'type': 'sessions',  'header': ['   Sessions:']       },
			\  { 'type': 'bookmarks', 'header': ['   Bookmarks:']      },
			\  { 'type': 'commands',  'header': ['   Commands:']       },
			\ ]

let g:startify_commands = [
			\   { 'up': [ 'Update Plugins', ':PlugUpdate' ] },
			\   { 'ug': [ 'Upgrade Plugin Manager', ':PlugUpgrade' ] },
			\ ]

let g:startify_bookmarks = [
    	\ { 'd': '/run/mount/diary' },
			\ { 'c': '~/.config/nvim/init.vim' },
			\ { 's': '~/st/config.h' },
			\ { 'x': '~/.Xresources' },
			\ { 'z': '~/.zshrc' },
			\ { 't': '~/.tmux.conf' }
			\ ]

autocmd User Startified setlocal cursorline
nmap <leader>st :Startify<cr>

"Airline

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'
let g:airline_powerline_fonts = 1
let g:airline_highlighting_cache = 1
let g:airline#extensions#keymap#enabled = 1 "Не показывать текущий маппинг
let g:airline_section_z = "\ue0a1:%l/%L Col:%c" "Кастомная графа положения курсора
let g:Powerline_symbols='unicode' "Поддержка unicode
let g:airline#extensions#xkblayout#enabled = 1
let g:airline_theme = 'monochrome' "alduin angr atomic ayu_dark ayu_light ayu_mirage badwolf behelit biogoo bubblegum cobalt2 cool dark_minimal desertink deus distinguished durant fairyfloss fruit_punch hybrid hybridline jellybeans jet kalisi kolor laederon light lucius luna minimalist molokai monochrome murmur night_owl onedark ouo owo papercolor peaksea powerlineish qwq raven ravenpower seagull seoul256 serene sierra silver simple soda sol solarized solarized_flood term term_light tomorrow ubaryd understated violet wombat xtermlight zenburn

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

autocmd BufEnter * :set relativenumber

nnoremap qp :QuartoPreview<CR><C-n>

augroup pandoc_syntax
    au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
    au! BufNewFile,BufFilePre,BufRead *.qmd set filetype=markdown.pandoc
    au! BufNewFile,BufFilePre,BufRead *.rmarkdown set filetype=markdown.pandoc
	augroup END

autocmd VimEnter *.md,*.qmd,*.R TagbarToggle
nnoremap nn :TagbarOpen fj<CR><C-n>
nnoremap nn :TagbarOpen fj<CR>j
let g:tagbar_right = 1
let g:tagbar_width = max([15, winwidth(0) / 5])
let g:tagbar_show_tag_linenumbers = 2
let g:tagbar_compact = 2
let g:tagbar_sort = 0

let g:tagbar_type_markdown = {
    \ 'ctagstype' : 'markdown',
    \ 'kinds' : [
        \ 'h:Heading_L1',
        \ 'i:Heading_L2',
        \ 'k:Heading_L3'
    \ ]
\ }

let g:tagbar_type_r = {
    \ 'ctagstype' : 'r',
    \ 'kinds'     : [
        \ 'f:Functions',
        \ 'g:GlobalVariables',
        \ 'v:FunctionVariables',
    \ ]
\ }
