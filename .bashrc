# .bashrc

export EDITOR=nvim
export VISUAL=nvim

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
PATH="$HOME/.local/bin:$HOME/bin:$PATH"
export PATH
# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

if [ "$COLORTERM" = "gnome-terminal" -a "$TERM" =~ st.* ]; then
    export TERM=gnome-256color
fi

if [ "$TERM" = "st" ] || [ "$TERM" = "st-256color" ]
then
    export TERM=st-256color
    export HAS_256_COLORS=yes
fi
if [ "$TERM" = "screen" ] && [ "$HAS_256_COLORS" = "yes" ]
then
    export TERM=screen-256color
fi

if [ -n "$MODULESHOME" ]; then
    module load neovim
    module load tmux/2.2
fi
