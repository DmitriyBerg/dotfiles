export ZSH=$HOME/.oh-my-zsh
#ZSH_THEME="zhann"
ZSH_THEME="lambda"

POWERLEVEL9K_MODE='nerdfont-fontconfig'

ZSH_TMUX_AUTOSTART="true"
ZSH_TMUX_AUTOSTART_ONCE="false"
ZSH_TMUX_AUTOCONNECT="false"
ZSH_TMUX_AUTOQUIT="true"
ZSH_TMUX_FIXTERM="false"
ZSH_TMUX_FIXTERM_WITH_256COLOR="true"

ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"

export KEYTIMEOUT=10
export BACKGROUND="light"

plugins=(
  git
  tmux
  sudo
  vi-mode
  vim-interaction web-search
  zsh-autosuggestions
	zsh-syntax-highlighting
  rust
  npm
  )

source $ZSH/oh-my-zsh.sh
export EDITOR="nvim"
#export TERM="xterm-256color"
export TERM="kitty"
PATH=$PATH:~/.cargo/bin
PATH=~/.cargo/bin:$PATH

unsetopt nomatch

alias vi="nvim"
alias vim="nvim"
alias md="mkdir -p"
alias rd="rmdir"
alias df="df -h"
alias mv="mv -i"
alias slink="link -s"
alias l="ls -l --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F"
alias ll="ls -lhtrdF .*"
alias la="ls -alF"
alias -s {go,txt,cfg,c,cpp,rb,asm,nim,conf,d,md}=nvim
alias -s {avi,mpeg,mpg,mov,m2v}=mpv
alias -s {png,jpeg,jpg,tiff,svg}=feh
alias grep="grep --color=auto"
alias cd..="cd .."
alias cd...="cd ../.."
alias cd....="cd ../../.."
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias gs="git status"
alias gsm="git summary"
alias ga='git add'
alias gaa='git add .'
alias gd='git diff'
alias gf='git fetch'
alias grv='git remote -v'
alias grb='git rebase'
alias gbr='git branch'
alias gpl="git pull"
alias gp="sudo git push"
alias gco="git checkout"
alias gl="git log"
alias gc="git commit -m"
alias gm="git merge"
alias gb="go build"
alias ta="tmux attach -t"
alias tad="tmux attach -d -t"
alias ts="tmux new-session -s"
alias tl="tmux list-sessions"
alias tksv="tmux kill-server"
alias tkss="tmux kill-session -t"
alias zyi="sudo zypper install"
alias zyr="sudo zypper rm -u"
alias zyc="sudo zypper clean -a"
alias zyu="sudo zypper ref && sudo zypper dup -y && sudo flatpak update -y"
alias zyf="sudo zypper refresh"
alias ytf="yt-dlp -F"
alias ytd="yt-dlp -f"
alias pip='noglob pip'
alias wt='wget -qO - v2.wttr.in/Йошкар-Ола'
alias neo="neofetch"
alias neoa="neofetch --ascii"
alias down="sudo shutdown -h now"
alias reboot="sudo shutdown -r now"
alias mount="sudo mount /dev/sdc1 /run/mount"
alias umount="sudo umount -l /run/mount"
alias rch="rm -rf ~/.vim/.backup/* ~/.vim/.swp/* ~/.cache/vivaldi/* ~/.cache/mozilla/* ~/.cache/chromium/* ~/.local/share/Trash/files/* ~/.local/share/Trash/info/* ~/.cache/ranger/*"
alias jcl="sudo journalctl --vacuum-time=1d"
alias clean="sudo zypper clean"
alias vol="alsamixer"
alias ra="ranger"
alias up="sudo wg-quick up wg0"
alias dwn="sudo wg-quick down wg0"
#alias up="nmcli c up ikev"
#alias dwn="nmcli c down ikev"

# Use lf to switch directories and bind it to ctrl-f
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^f' 'lfcd\n'

fpath+=${ZDOTDIR:-~}/.zsh_functions
#source <(tdl completion zsh)
