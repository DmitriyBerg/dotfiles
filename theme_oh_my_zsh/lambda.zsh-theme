PROMPT='%{$fg_bold[red]%}%{$fg_bold[green]%}% |> %~/ $(git_prompt_info)%{$reset_color%}'
# ❱
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
